import os
import re
import subprocess

from psutil import Process

INPUT_BASE_DIR = "M:\\Plex Media\\Anime\\"
PRIORITY_INPUT_PATH = "M:\\Plex Media\\Anime\\{show}\\"
OUTPUT_PATH = "D:\\Encoded Anime\\"
PRIORITY_FILE = ".\\priority.txt"
EXCLUDE_FILE = ".\\exclude.txt"

MKV_INFO_CMD = "mkvinfo"
MKV_EXTRACT_CMD = "mkvextract"
FFMPEG_CMD = "ffmpeg -i \"{mkv}\" -movflags faststart {sub_arg} -pix_fmt yuv420p -map 0:v -map 0:a " \
             "-c:v {v_codec} -preset {preset} -tune {tune} -profile:v {profile} -level {level} -crf {crf} " \
             "-c:a copy \"{output}\""

FFMPEG_CMD_NEW = "ffmpeg -i \"{mkv}\" -movflags faststart -pix_fmt yuv420p -map 0:v -map 0:a " \
                 "-c:v {v_codec} -preset {preset} -tune {tune} -profile:v {profile} -level {level} " \
                 "-crf {crf} -c:a copy \"{output}\""

SUB_ARG = "-vf \"ass={sub_track}\""

V_CODEC = "libx264"
PRESET = "veryslow"
TUNE = "animation"
PROFILE = "high"
LEVEL = "4.1"
CRF = "21"

MAX_QUEUE_SIZE = 1
STOP_ON_ERROR = True
PROCESS_PRIORITY = subprocess.NORMAL_PRIORITY_CLASS
CPU_AFFINITY = []


class Episode:
    origin = ""
    episode_name = ""
    tracks = []


class Track:
    track_number = -1
    track_type = ""
    track_language = ""

    def __init__(self, num, t_type, lang):
        self.track_number = num
        self.track_type = t_type
        self.track_language = lang


def get_mkv_info(mkv_file):
    args = MKV_INFO_CMD + " \"" + mkv_file + "\""

    comp_proc = subprocess.run(args, shell=True, capture_output=True, check=STOP_ON_ERROR)

    return comp_proc.stdout.decode("utf-8").splitlines()


def populate_tracks_for_episode(episode, mkv_file):
    dec_info = get_mkv_info(mkv_file)

    track_number = -1
    track_type = ""
    track_language = ""
    track_name = ""

    for line in dec_info:
        if "Track number" in line:
            track_number = int(re.sub('[A-Za-z_:()& +|]', '', line)[1:])
        if "Track type" in line:
            track_type = "audio" if "audio" in line else "subtitles" if "subtitles" in line else ""
        if "Language" in line:
            track_language = "jpn" if "jpn" in line else "en" if "en" in line else "en" if track_type == "subtitles" \
                else "und"
        if "Name" in line:
            track_name = line[12:]

        if track_number != -1 and track_type != "":
            track_language = track_name if track_language == "" and track_name != "" else track_language
            episode.tracks.append(Track(track_number, track_type, track_language))
            track_number = -1
            track_type = ""
            track_language = ""
            track_name = ""


def create_episode_object(mkv_file):
    episode = Episode()
    episode.origin = mkv_file
    episode.episode_name = get_episode_name(mkv_file)
    populate_tracks_for_episode(episode, mkv_file)

    return episode


def read_file(file_name):
    with open(file_name, "r") as in_file:
        encode_list = [string.rstrip('\n') for string in in_file.readlines()]
        in_file.close()
        return encode_list


def get_anime_to_encode(show_path, episode_list):
    for root, dirs, files in os.walk(show_path):
        for in_file in files:
            if EXCLUDE_LIST:
                if any(show in in_file for show in EXCLUDE_LIST):
                    continue
            if in_file.endswith(".mkv") and len(episode_list) < MAX_QUEUE_SIZE:
                path = os.path.join(root, in_file)

                if not any(episode.origin in path for episode in episode_list):
                    episode_list.append(create_episode_object(path))


def get_episode_name(string):
    k = string.rfind("\\") + 1
    strip_path = string[k:]
    k = strip_path.rfind(".mkv")
    strip_ext = strip_path[:k]
    return strip_ext


def run_extract(mkv, tracknum, outfile):
    if os.path.isfile(outfile):
        print("\"" + outfile + "\" exists, skipping")
        return

    extract = MKV_EXTRACT_CMD + " \"{mkv}\" tracks {etrack}:\"{out}\"".format(mkv=mkv,
                                                                              etrack=tracknum,
                                                                              out=outfile)
    subprocess.run(extract, shell=True, check=STOP_ON_ERROR)


def extract_tracks(episode):
    sub_tracks = [track for track in episode.tracks if track.track_type == "subtitles"]
    # audio_tracks = [track for track in episode.tracks if track.track_type == "audio"]

    for track in sub_tracks:
        outfile = OUTPUT_PATH + episode.episode_name + "_" + track.track_language + "_" + track.track_number + ".ass"
        run_extract(episode.origin, track.track_number, outfile)


def encode_episode_to_mp4(episode):
    outfile = OUTPUT_PATH + episode.episode_name + ".mp4"

    if os.path.exists(outfile):
        print("\"" + outfile + "\" exists, skipping...")
        return

    extract_tracks(episode)

    ffmpeg = FFMPEG_CMD_NEW.format(mkv=episode.origin, v_codec=V_CODEC, preset=PRESET, tune=TUNE, profile=PROFILE,
                                   level=LEVEL, crf=CRF, output=outfile)

    print(ffmpeg)

    try:
        subprocess.run(ffmpeg, shell=True, check=STOP_ON_ERROR, creationflags=PROCESS_PRIORITY)

    except subprocess.CalledProcessError:
        remove_trash_on_fail(episode)


def remove_trash_on_fail(episode):
    for root, dirs, files in os.walk(OUTPUT_PATH):
        for in_file in files:
            if episode.episode_name in in_file:
                path = os.path.join(root, in_file)
                os.remove(path)


def main():
    enc_list = read_file(PRIORITY_FILE)

    episode_list = []

    for name in enc_list:
        path = PRIORITY_INPUT_PATH.format(show=name)
        get_anime_to_encode(path, episode_list)

    get_anime_to_encode(INPUT_BASE_DIR, episode_list)

    for episode in episode_list:
        encode_episode_to_mp4(episode)


if __name__ == "__main__":
    EXCLUDE_LIST = read_file(EXCLUDE_FILE)

    if CPU_AFFINITY:
        Process().cpu_affinity(CPU_AFFINITY)

    main()
